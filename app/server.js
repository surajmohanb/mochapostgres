const express = require('express')
const bodyParser = require('body-parser')
const Pool = require('pg').Pool
const app = express()
const port = 3000

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API' })
})


let pool;
function connectToPostgres() {
    pool = new Pool({
        user: 'sunshine',
        host: 'localhost',
        database: 'hellodb',
        password: 'sunshine',
        port: 5432,
    })
}

const getUsers = (request, response) => {
    pool.query('SELECT * FROM sdb ORDER BY id ASC', (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

app.get('/users', getUsers);

const getUserById = (request, response) => {
    const id = parseInt(request.params.id)

    pool.query('SELECT * FROM sdb WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

app.get('/user/:id', getUserById);

const createUser = (request, response) => {
    const { name, price } = request.body

    pool.query('INSERT INTO sdb (name, price, last_update) VALUES ($1, $2, $3) RETURNING *', [name, price, new Date()], (error, result) => {
        if (error) {
            throw error
        }
        response.status(201).json(result.rows[0])
    })
}

app.post('/user', createUser);


const updateUser = (request, response) => {
    const id = parseInt(request.params.id)
    const { name, price } = request.body

    pool.query(
        'UPDATE sdb SET name = $1, price = $2 WHERE id = $3 RETURNING *',
        [name, price, id],
        (error, results) => {
            if (error) {
                throw error
            }
            response.status(200).json(results.rows[0])
        }
    )
}

app.put('/user/:id', updateUser);



const deleteUser = (request, response) => {
    const id = parseInt(request.params.id)

    pool.query('DELETE FROM sdb WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json({
            deletedId:id
        })
    })
}

app.delete('/user/:id', deleteUser);

const deleteAllUser = (request, response) => {

    pool.query('TRUNCATE TABLE sdb RESTART IDENTITY;', (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).send(`All Users Deleted`)
    })
}

app.delete('/delall', deleteAllUser);


connectToPostgres();
if(process.env.DEV_ENV != "test"){
    app.listen(port, () => {
        console.log(`App running on port ${port}.`)
    })
}
else{
    module.exports = app;
}
