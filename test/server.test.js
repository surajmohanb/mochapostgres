var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app/server');
var should = chai.should();
const expect = chai.expect
chai.use(chaiHttp);

describe('Postgres SQL : Nodejs', function () {

    it('Empty Get Call / GET', function (done) {
        chai.request(server)
            .get('/')
            .end(function (err, res) {
                res.should.have.status(200);
                done();
            });
    });

    it('Refresh Local DB /delall DELETE', function (done) {
        chai.request(server)
            .delete('/delall')
            .end(function (err, res) {
                res.should.have.status(200);
                done();
            });
    });

    it('Make /users GET', function (done) {
        chai.request(server)
            .get('/users')
            .end(function (err, res) {
                res.should.have.status(200);
                expect(res.body).to.have.length(0);
                done();
            });
    });

    it('Create New User /user POST', function (done) {
        const userObject = {
            "name": "User1",
            "price": "12.56",
            "last_update": new Date()
        }
        chai.request(server)
            .post('/user')
            .send(userObject)
            .end(function (err, res) {
                res.should.have.status(201);
                expect(res.body.id).equal(1);
                expect(res.body.name).equal("User1")
                done();
            });
    });

    it('Make /users GET : Length should be 1', function (done) {
        chai.request(server)
            .get('/users')
            .end(function (err, res) {
                res.should.have.status(200);
                expect(res.body).to.have.length(1);
                done();
            });
    });

    it('Update User /user PUT', function (done) {
        const userObject = {
            "name": "UserUpdated",
            "price": "12.56",
            "last_update": new Date()
        }
        chai.request(server)
            .put('/user/1')
            .send(userObject)
            .end(function (err, res) {
                res.should.have.status(200);
                expect(res.body.id).equal(1);
                expect(res.body.name).equal("UserUpdated")
                done();
            });
    });

    it('Update User /user DELETE', function (done) {

        chai.request(server)
            .delete('/user/1')
            .end(function (err, res) {
                res.should.have.status(200);
                expect(res.body.deletedId).equal(1);
                done();
            });
    });
});